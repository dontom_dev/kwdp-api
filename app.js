var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cron = require('node-cron');
var request = require('request');
var fs = require('fs');
var cors = require('cors');

var openSkyNetworkApiFile = 'data/open_sky_network_api.json';
var port = process.env.PORT || 8080;

var app = express();
app.use(bodyParser.json());
app.use(cors());

Airplane = require('./models/airplane');
Time = require('./models/time');

// Connect to Mongoose
var db_username = process.env.DB_USERNAME;
var db_password = process.env.DB_PASSWORD;
mongoose.connect('mongodb://' + db_username + ':' + db_password + '@ds117189.mlab.com:17189/flights_monitoring');
var db = mongoose.connection;

var username = process.env.OPENSKY_API_USERNAME;
var password = process.env.OPENSKY_API_PASSWORD;
var _url = "https://" + username + ":" + password + "@opensky-network.org/api/states/all";
var _kwdpUrl = "https://still-stream-74569.herokuapp.com";

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

// Getting data only for those planes from db
var planes = []; // plane list
function getAirplanes() {
  Airplane.getAirplanes(function(err, airplanes) {
    planes = [];
    if(err){
      throw err;
    }
    airplanes.forEach(plane => {
      planes.push(plane.icao24);
    });
  }); 
}
getAirplanes();

var flightDetails;
var currentTime;
cron.schedule('* * * * * *', function(){ // every second
  getAirplanes();
  planes.forEach(element => { 
    // Get response from Open Sky network API
    request.get(_url + "?icao24=" + element, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        flightDetails = JSON.parse(body);
        currentTime = new Date();

        //** Post request to my custom API - saving in db
        time = new Time();
        time.plane_id = element;

        if(flightDetails['states']){
          time.connection_time = Math.floor((currentTime / 1000)) - flightDetails['states'][0][4];       
        }
        else{
          time.connection_time = 0;
        }

        time.create_date = currentTime;
        Time.addTime(time, function(err, time) {
          if(err){
            throw err;
          }
        });
      }
    });
  });
  
});

// main GET request
app.get('/', function(req, res, next){
  res.send('Server working...');
});

app.post('/', function(req, res, next) {
  res.send('POST request...');
 });

// GET all Airplanes
app.get('/api/airplanes', function(req, res){
  Airplane.getAirplanes(function(err, airplanes) {
    if(err){
      throw err;
    }
    res.json(airplanes);
  });
});

// GET Airplane by Plane Id
app.get('/api/airplanes/:_id', function(req, res){
  Airplane.getAirplaneByPlaneId(req.params._id, function(err, airplane) {
    if(err){
      throw err;
    }
    res.json(airplane);
  });
});

// POST add Airplane
app.post('/api/airplanes', function(req, res){
  var airplane = req.body;
  Airplane.addAirplane(airplane, function(err, airplane) {
    if(err){
      throw err;
    }
    res.json(airplane);
  });
});

// GET all Times
app.get('/api/times', function(req, res){
  Time.getTimes(function(err, times) {
    if(err){
      throw err;
    }
    res.json(times);
  });
});

// GET N Times
app.get('/api/times/limit/:_lim', function(req, res){
  Time.getNTimes(req.params._lim, function(err, times) {
    if(err){
      throw err;
    }
    res.json(times);
  });
});

// GET Times by Plane Id
app.get('/api/times/:_id', function(req, res){
  Time.getTimesByPlaneId(req.params._id, function(err, time) {
    if(err){
      throw err;
    }
    res.json(time);
  });
});

// GET N Times by Plane Id
app.get('/api/times/:_id/limit/:_lim', function(req, res){
  Time.getNTimesByPlaneId(req.params._id, req.params._lim, function(err, time) {
    if(err){
      throw err;
    }
    res.json(time);
  });
});

// GET newest Time by Plane Id
// GET Time by Plane Id
app.get('/api/times/:_id/newest', function(req, res){
  Time.getTimeByPlaneId(req.params._id, function(err, time) {
    if(err){
      throw err;
    }
    res.json(time);
  });
});

// POST add Time
app.post('/api/times', function(req, res){
  var time = req.body;
  Time.addTime(time, function(err, time) {
    if(err){
      throw err;
    }
    res.json(time);
  });
});

app.listen(port, function() {
  console.log('Application running');
});
