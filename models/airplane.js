var mongoose = require('mongoose');

// Airplane schema for application
var airplaneSchema = mongoose.Schema({
  icao24:{
    type: String,
    required: true
  },
  origin_country:{
    type: String,
    require: true
  },
  create_date:{
    type: Date,
    default: Date.now
  }
});

// make object accessible from any place
var Airplane = module.exports = mongoose.model('Airplane', airplaneSchema);

// GET Airplanes
module.exports.getAirplanes = function(callback){
  Airplane.find(callback);
};

// GET Airplane by Id
module.exports.getAirplaneByPlaneId = function(_id, callback){
  Airplane.find({'icao24':_id}, callback);
};

// POST Add Airplane
module.exports.addAirplane = function(airplane, callback){
  Airplane.create(airplane, callback);
};