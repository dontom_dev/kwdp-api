var mongoose = require('mongoose');

// Time schema for application
var timeSchema = mongoose.Schema({
  plane_id:{
    type: String,
    required: true
  },
  connection_time:{
    type: String,
    required: true
  },
  create_date:{
    type: Date,
    default: Date.now
  }
});

// make object accessible from any place
var Time = module.exports = mongoose.model('Time', timeSchema);

// GET Times
module.exports.getTimes = function(callback){
  Time.find(callback);
};

// GET N Times
module.exports.getNTimes = function(_lim, callback){
  Time.find(callback).sort({$natural: -1}).limit(parseInt(_lim));
};

// GET Times by Id
module.exports.getTimesByPlaneId = function(_id, callback){
  Time.find({'plane_id':_id}, callback);
};

// GET N Times by Id
module.exports.getNTimesByPlaneId = function(_id, _lim, callback){
  Time.find({'plane_id':_id}, callback).sort({$natural: -1}).limit(parseInt(_lim));
};

// POST Add Time
module.exports.addTime = function(time, callback){
  Time.create(time, callback);
};